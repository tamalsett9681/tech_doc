============================================= X11 Forwarding for Oracle =======================================================
X11Forwarding yes
X11DisplayOffset 10
X11UseLocalhost yes

# cat /etc/ssh/sshd_config | grep -i AddressFamily 
AddressFamily inet

systemctl restart sshd

yum install xorg-x11-xauth xorg-x11-fonts-* xorg-x11-utils xorg-x11-apps

[tcsuser@WEFNOLVMDBTST01 ~]$ xauth list
WEFNOLVMDBTST01/unix:10  MIT-MAGIC-COOKIE-1  0121f08dd6f9537eba222a4e543c506c
[tcsuser@WEFNOLVMDBTST01 ~]$ sudo su -
Last login: Thu Jan 31 10:39:10 CET 2019 on pts/3
[root@WEFNOLVMDBTST01 ~]# touch ~/.Xauthority
[root@WEFNOLVMDBTST01 ~]# xauth add WEFNOLVMDBTST01/unix:10  MIT-MAGIC-COOKIE-1  0121f08dd6f9537eba222a4e543c506c
[root@WEFNOLVMDBTST01 ~]# logout
[tcsuser@WEFNOLVMDBTST01 ~]$ sudo su -
Last login: Thu Jan 31 10:43:46 CET 2019 on pts/3
[root@WEFNOLVMDBTST01 ~]# xclock
Warning: Missing charsets in String to FontSet conversion
[root@WEFNOLVMDBTST01 ~]#

