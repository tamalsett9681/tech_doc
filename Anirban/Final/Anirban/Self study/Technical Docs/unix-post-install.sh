systemctl stop firewalld
systemctl disable firewalld
sed -i '7s/.*/SELINUX=disabled/' /etc/sysconfig/selinux
sed -i '7s/.*/SELINUX=disabled/' /etc/selinux/config
setenforce 0
/usr/sbin/useradd -u 1201 -c "TCS System Admin" tcsuser
/bin/echo "tcsuser ALL=(ALL) ALL" >> /etc/sudoers
/usr/sbin/useradd -u 1101 -c "Addm User" addm
/usr/sbin/useradd -u 1102 -c "Bppm User" bppm
/usr/sbin/useradd -u 1103 -c "Bppmpu User" bppmpu
/usr/sbin/useradd -u 1104 -c "Backup User" bkpadmin
/bin/echo Tcs@unix1|passwd tcsuser --stdin
/bin/echo Tcs@2009|passwd addm --stdin
/bin/echo Tryg@123|passwd bppm --stdin
/bin/echo Tryg@123|passwd bppmpu --stdin
/bin/echo Tryg@1234|passwd bkpadmin --stdin
/bin/echo "bkpadmin ALL=(ALL) ALL" >> /etc/sudoers
/bin/echo "bppmpu ALL=(ALL) ALL" >> /etc/sudoers
/bin/echo "addm ALL=(ALL) ALL" >> /etc/sudoers
/bin/timedatectl set-timezone Europe/Copenhagen
/bin/sed -i '6s/.*/PEERDNS=no/' /etc/sysconfig/network-scripts/ifcfg-eth0
/bin/echo "DNS1=10.90.1.11" >> /etc/sysconfig/network-scripts/ifcfg-eth0
/bin/echo "DNS2=10.80.1.11" >> /etc/sysconfig/network-scripts/ifcfg-eth0
/bin/echo "DNS3=10.48.22.11" >> /etc/sysconfig/network-scripts/ifcfg-eth0
/bin/echo "DOMAIN=prd1.prdroot.net" >> /etc/sysconfig/network-scripts/ifcfg-eth0
/usr/bin/systemctl restart network
/bin/yum install -y ntp telnet wget sssd realmd oddjob oddjob-mkhomedir adcli samba-common samba-common-tools krb5-workstation
/usr/bin/systemctl start oddjobd
/usr/bin/systemctl enable oddjobd
/sbin/authconfig --enablemkhomedir �update
/bin/sed -i '21s/.*/#&/' /etc/ntp.conf
/bin/sed -i '22s/.*/#&/' /etc/ntp.conf
/bin/sed -i '23s/.*/#&/' /etc/ntp.conf
/bin/sed -i '24s/.*/#&/' /etc/ntp.conf
/bin/echo "server 10.90.1.16" >> /etc/ntp.conf
/bin/echo "server 10.80.1.16" >> /etc/ntp.conf
/usr/bin/systemctl stop chronyd
/usr/bin/systemctl disable chronyd
/usr/bin/systemctl restart ntpd
/usr/bin/systemctl enable ntpd
/sbin/ntpdate -u 10.90.1.16
/bin/sed -i '25s/.*/PASS_MAX_DAYS   90/' /etc/login.defs
/bin/sed -i '26s/.*/PASS_MIN_DAYS   7/' /etc/login.defs
/bin/sed -i '27s/.*/PASS_MIN_LEN    8/' /etc/login.defs
/bin/sed -i '28s/.*/PASS_WARN_AGE   28/' /etc/login.defs
/bin/sed -i '29s/.*/LOGIN_RETRIES   5\n/' /etc/login.defs
/bin/sed -i '6s/.*/auth            required        pam_wheel.so root_only use_uid debug/' /etc/pam.d/su
/sbin/authconfig --passalgo=sha512 --passminlen=9 --passminclass=3 --passmaxrepeat=3 --passmaxclassrepeat=4 --enablerequpper --enablereqlower --enablereqdigit --enablereqother --enablefaillock  --faillockargs="audit deny=5 fail_interval=900 unlock_time=0" --update
/bin/echo "difok = 5" >> /etc/security/pwquality.conf
/bin/echo "gecoscheck = 1" >> /etc/security/pwquality.conf
/bin/sed -i '/pam_pwquality.so/a password    requisite     pam_pwhistory.so debug use_authtok remember=10 retry=3' /etc/pam.d/password-auth
/bin/sed -i '/pam_pwquality.so/a password    requisite     pam_pwhistory.so debug use_authtok remember=10 retry=3' /etc/pam.d/system-auth
/bin/echo "session     required      pam_tty_audit.so enable=*" >> /etc/pam.d/system-auth
/bin/echo "session     required      pam_tty_audit.so enable=*" >> /etc/pam.d/password-auth
/bin/sed -i '81i\Defaults    logfile = /var/log/sudo.log' /etc/sudoers
/bin/sed -i '39i\PermitRootLogin no' /etc/ssh/sshd_config
/bin/sed -i '113s/.*/ClientAliveInterval 3600/' /etc/ssh/sshd_config
/bin/sed -i '114s/.*/ClientAliveCountMax 0/' /etc/ssh/sshd_config
/bin/echo "Ciphers aes128-ctr,aes192-ctr,aes256-ctr" >> /etc/ssh/sshd_config
/bin/echo "MACs hmac-sha1,umac-64@openssh.com,hmac-ripemd160" >> /etc/ssh/sshd_config
/bin/echo "Ciphers aes128-ctr,aes192-ctr,aes256-ctr" >> /etc/ssh/ssh_config
/bin/echo "MACs hmac-sha1,umac-64@openssh.com,hmac-ripemd160" >> /etc/ssh/ssh_config
/bin/sed -i 's/GSSAPIAuthentication yes/GSSAPIAuthentication no/' /etc/ssh/sshd_config
/bin/sed -i '12s/.*/max_log_file = 256/' /etc/audit/auditd.conf
/bin/sed -i '13s/.*/num_logs = 7/' /etc/audit/auditd.conf
/usr/bin/systemctl enable auditd
/bin/sed -i 's/HISTSIZE=1000/HISTSIZE=5000/g' /etc/profile
/bin/echo "shopt -s cmdhist" >> /etc/bashrc
/bin/echo 'export HISTTIMEFORMAT="%d-%m-%Y %T "' >> /etc/bashrc
/bin/echo 'export HISTFILESIZE=5000' >> /etc/bashrc
/bin/echo 'export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$"\n"}history -a; history -c; history -r"' >> /etc/bashrc
/usr/sbin/useradd -u 1202 -c "Subhayan Das" tatasds
/usr/sbin/useradd -u 1203 -c "Tamal Sett" tatatas
/usr/sbin/useradd -u 1204 -c "Anirban Adak" tataana
/bin/echo Passw0rd@123|passwd tatasds --stdin
/bin/echo Passw0rd@123|passwd tataana --stdin
/bin/echo Passw0rd@123|passwd tatatas --stdin
/bin/echo "tatasds ALL=(ALL) ALL" >> /etc/sudoers
/bin/echo "tatatas ALL=(ALL) ALL" >> /etc/sudoers
/bin/echo "tataana ALL=(ALL) ALL" >> /etc/sudoers
/bin/wget -q -O /etc/pki/ca-trust/source/anchors/ZscalerRootCertificate-2048-SHA256.crt http://10.80.1.16/ZscalerRootCertificate-2048-SHA256.crt 
/bin/update-ca-trust extract
/bin/wget -q -O /etc/systemd/system/bppm.service http://10.80.1.16/bppm.service
/bin/chmod +x /etc/systemd/system/bppm.service
/bin/yum -y update
/bin/mkdir /opt/bmc
/bin/chown bppm:bppm /opt/bmc
/bin/mkdir /commvault
/bin/echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
/bin/echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf
/sbin/sysctl -p
/bin/wget -q -O /home/tcsuser/flexera.sh http://10.80.1.16/flexera.sh
/bin/sh /home/tcsuser/flexera.sh
/bin/wget -q -O /home/tcsuser/bigfix.sh http://10.80.1.16/bigfix.sh
/bin/sh /home/tcsuser/bigfix.sh

/sbin/authconfig --update --updateall


If disk is addedd
/bin/wget -q -O /home/tcsuser/disk.sh http://10.80.1.16/disk.sh
/bin/sh /home/tcsuser/disk.sh

---------------------------------------------------------------
# realm join --computer-ou="ou=Azure,ou=Servers,ou=TCS,dc=prd1,dc=prdroot,dc=net" --user tataasi prd1.prdroot.net --install=/
 ok....done,,,
 #vi /etc/sssd/sssd.conf

qualifiedname =False
homedirectry  == /home/%u
entry 
#systemctl restart sssd
 #realm deny --all
 #realm discover prd1.prdroot.net
 
 #realm permit T01EDJO
 #realm permit T01JAOL
 visudo
 #realm discover prd1.prdroot.net
 su - t01edjo



