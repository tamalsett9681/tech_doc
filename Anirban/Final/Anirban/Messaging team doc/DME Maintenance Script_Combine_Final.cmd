@echo off

title DME Script authentication

color 3

:MAIN

cls
echo.

Echo.

Echo ================================================================================

Echo  DME Script Authentication System Coded by Sudhagar.rajaraman@tryg.dk (TATASUR) 

Echo ================================================================================

Echo.

echo Enter your username
echo.
set /p "username= > "
if %username%==DME goto PASSWORD
goto ERROR1

:ERROR1
echo.
echo The username was not found, please try again.
echo.
pause
goto MAIN

:PASSWORD

echo.
echo Enter your password
echo.
set /p "password= > "
if %password%==DME@420 goto OK
goto ERROR2

:ERROR2
echo.
echo The password is incorrect, please try again.
echo.
pause
goto PASSWORD

:OK
REM Do your thing here..

cls
echo.

color 3

TITLE DME Automation Tool

Echo.

Echo ==============================================================================

Echo  DME Automation Tool Coded by Sudhagar.rajaraman@tryg.dk (TATASUR) 

Echo ==============================================================================

echo.

set /p usr=Enter your Username ( prd1\abc ) : 
set /p pwd=Enter your Password : 
echo.

echo *******

echo.

set /p lusr=Enter your Local Username ( abc ) : 
set /p lpwd=Enter your Password : 

cls


:Choose

cls
Echo ~~~~~~~~~~~~~ TCS TRYG Messaging and Collaboration Team ~~~~~~~~~~~~~~

set datetimef=%date:~-4%_%date:~3,2%_%date:~0,2%-%time:~0,2%_%time:~3,2%

echo.
echo.
echo.

echo =====================================================

echo 1. DME Service Checker

echo 2. DME Service STOP Sequence

echo 3. DME Sequential Reboot 

echo 4. DME RDP

echo 5. Exit

echo =====================================================

echo.

set /p role=Choose your option given above:

if %role%==1 goto DME_Check
if %role%==2 goto DME_STOP
if %role%==3 goto DME_Restart
if %role%==4 goto DME_RDP
if %role%==5 goto DME_EX

exit

:DME_Check

Echo ~~~~~~~~~~~~~ TCS TRYG Messaging and Collaboration Team ~~~~~~~~~~~~~~

set datetimef=%date:~-4%_%date:~3,2%_%date:~0,2%-%time:~0,2%_%time:~3,2%

echo ================================================================ > DME_Checker.log

echo *** 10.48.19.181 *** >> DME_Checker.log

PsService \\10.48.19.181 -u "%usr%" -p "%pwd%" query "BMC Patrol Agent" >> DME_Checker.log
PsService \\10.48.19.181 -u "%usr%" -p "%pwd%" query "DME Connector" >> DME_Checker.log

echo ================================================================ >> DME_Checker.log

echo *** 10.59.19.139 *** >> DME_Checker.log

PsService \\10.59.19.139 -u "%usr%" -p "%pwd%" query "BMC Patrol Agent" >> DME_Checker.log
PsService \\10.59.19.139 -u "%usr%" -p "%pwd%" query "DME Connector" >> DME_Checker.log

echo ================================================================ >> DME_Checker.log

echo *** 10.48.19.64 *** >> DME_Checker.log

PsService \\10.48.19.64 -u "%usr%" -p "%pwd%" query "BMC Patrol Agent" >> DME_Checker.log
PsService \\10.48.19.64 -u "%usr%" -p "%pwd%" query "DME Connector" >> DME_Checker.log

echo ================================================================ >> DME_Checker.log

echo *** 10.59.19.24 *** >> DME_Checker.log

PsService \\10.59.19.24 -u "%usr%" -p "%pwd%" query "BMC Patrol Agent" >> DME_Checker.log
PsService \\10.59.19.24 -u "%usr%" -p "%pwd%" query "DME Connector" >> DME_Checker.log

echo ================================================================ >> DME_Checker.log

echo *** 10.48.19.182 *** >> DME_Checker.log

PsService \\10.48.19.182 -u "%usr%" -p "%pwd%" query "DMEconnectorDK" >> DME_Checker.log
PsService \\10.48.19.182 -u "%usr%" -p "%pwd%" query "DMEconnectorDKMO" >> DME_Checker.log
PsService \\10.48.19.182 -u "%usr%" -p "%pwd%" query "DMEconnectorNO" >> DME_Checker.log

echo ================================================================ >> DME_Checker.log

echo *** 10.59.19.140 *** >> DME_Checker.log

PsService \\10.59.19.140 -u "%usr%" -p "%pwd%" query "DMEconnectorDK" >> DME_Checker.log
PsService \\10.59.19.140 -u "%usr%" -p "%pwd%" query "DMEconnectorDKMO" >> DME_Checker.log
PsService \\10.59.19.140 -u "%usr%" -p "%pwd%" query "DMEconnectorNO" >> DME_Checker.log

echo ================================================================ >> DME_Checker.log

echo *** 10.48.160.87 *** >> DME_Checker.log

PsService \\10.48.160.87 -u "%lusr%" -p "%lpwd%" query "BMC Patrol Agent" >> DME_Checker.log
PsService \\10.48.160.87 -u "%lusr%" -p "%lpwd%" query "DMEServer" >> DME_Checker.log

echo ================================================================ >> DME_Checker.log

echo *** 10.59.160.83 *** >> DME_Checker.log

PsService \\10.59.160.83 -u "%lusr%" -p "%lpwd%" query "BMC Patrol Agent" >> DME_Checker.log
PsService \\10.59.160.83 -u "%lusr%" -p "%lpwd%" query "DMEServer" >> DME_Checker.log

echo ================================================================ >> DME_Checker.log

pause
Goto Choose

:DME_STOP
Echo ~~~~~~~~~~~~~ TCS TRYG Messaging and Collaboration Team ~~~~~~~~~~~~~~

set datetimef=%date:~-4%_%date:~3,2%_%date:~0,2%-%time:~0,2%_%time:~3,2%


psexec \\10.48.19.181 -u "%usr%" -p "%pwd%" net stop "BMC Patrol Agent"
psexec \\10.48.19.181 -u "%usr%" -p "%pwd%" net stop "DME Connector"

psexec \\10.59.19.139 -u "%usr%" -p "%pwd%" net stop "BMC Patrol Agent"
psexec \\10.59.19.139 -u "%usr%" -p "%pwd%" net stop "DME Connector"

psexec \\10.48.19.64 -u "%usr%" -p "%pwd%" net stop "BMC Patrol Agent"
psexec \\10.48.19.64 -u "%usr%" -p "%pwd%" net stop "DME Connector"

psexec \\10.59.19.24 -u "%usr%" -p "%pwd%" net stop "BMC Patrol Agent"
psexec \\10.59.19.24 -u "%usr%" -p "%pwd%" net stop "DME Connector"

psexec \\10.48.19.182 -u "%usr%" -p "%pwd%" net stop "BMC Patrol Agent"
psexec \\10.48.19.182 -u "%usr%" -p "%pwd%" net stop "DMEconnectorDK"
psexec \\10.48.19.182 -u "%usr%" -p "%pwd%" net stop "DMEconnectorDKMO"
psexec \\10.48.19.182 -u "%usr%" -p "%pwd%" net stop "DMEconnectorNO"

psexec \\10.59.19.140 -u "%usr%" -p "%pwd%" net stop "BMC Patrol Agent"
psexec \\10.59.19.140 -u "%usr%" -p "%pwd%" net stop "DMEconnectorDK"
psexec \\10.59.19.140 -u "%usr%" -p "%pwd%" net stop "DMEconnectorDKMO"
psexec \\10.59.19.140 -u "%usr%" -p "%pwd%" net stop "DMEconnectorNO"

psexec \\10.48.160.87 -u "%lusr%" -p "%lpwd%" net stop "BMC Patrol Agent"
psexec \\10.48.160.87 -u "%lusr%" -p "%lpwd%" net stop "DMEServer"

psexec \\10.59.160.83 -u "%lusr%" -p "%lpwd%" net stop "BMC Patrol Agent"
psexec \\10.59.160.83 -u "%lusr%" -p "%lpwd%" net stop "DMEServer"

pause
Goto Choose

:DME_Restart
Echo ~~~~~~~~~~~~~ TCS TRYG Messaging and Collaboration Team ~~~~~~~~~~~~~~

set datetimef=%date:~-4%_%date:~3,2%_%date:~0,2%-%time:~0,2%_%time:~3,2%

psexec \\10.59.160.83 -u "%lusr%" -p "%lpwd%" shutdown /r /t 01

TIMEOUT /T 60 /NOBREAK

psexec \\10.48.160.87 -u "%lusr%" -p "%lpwd%" shutdown /r /t 01

TIMEOUT /T 60 /NOBREAK

psexec \\10.59.19.140 -u "%usr%" -p "%pwd%" shutdown /r /t 01

TIMEOUT /T 60 /NOBREAK

psexec \\10.48.19.182 -u "%usr%" -p "%pwd%" shutdown /r /t 01

TIMEOUT /T 60 /NOBREAK

psexec \\10.59.19.24 -u "%usr%" -p "%pwd%" shutdown /r /t 01

TIMEOUT /T 60 /NOBREAK

psexec \\10.48.19.64 -u "%usr%" -p "%pwd%" shutdown /r /t 01

TIMEOUT /T 60 /NOBREAK

psexec \\10.59.19.139 -u "%usr%" -p "%pwd%" shutdown /r /t 01

TIMEOUT /T 60 /NOBREAK

psexec \\10.48.19.181 -u "%usr%" -p "%pwd%" shutdown /r /t 01

pause
Goto Choose

:DME_RDP

cmdkey /generic:10.59.160.83 /user:"%lusr%" /pass:"%lpwd%"
start mstsc /admin /v 10.59.160.83


cmdkey /generic:10.48.160.87 /user:"%lusr%" /pass:"%lpwd%"
start mstsc /admin /v 10.48.160.87


cmdkey /generic:10.48.19.181 /user:"%usr%" /pass:"%pwd%"
start mstsc /admin /v 10.48.19.181


cmdkey /generic:10.48.19.64 /user:"%usr%" /pass:"%pwd%"
start mstsc /admin /v 10.48.19.64


cmdkey /generic:10.59.19.24 /user:"%usr%" /pass:"%pwd%"
start mstsc /admin /v 10.59.19.24


cmdkey /generic:10.59.19.139 /user:"%usr%" /pass:"%pwd%"
start mstsc /admin /v 10.59.19.139


cmdkey /generic:110.48.19.182 /user:"%usr%" /pass:"%pwd%"
start mstsc /admin /v 10.48.19.182


cmdkey /generic:10.59.19.140 /user:"%usr%" /pass:"%pwd%"
start mstsc /admin /v 10.59.19.140

TIMEOUT /T 05 /NOBREAK

cmdkey.exe /list > "%TEMP%\List.txt"
findstr.exe Target "%TEMP%\List.txt" > "%TEMP%\tokensonly.txt"
FOR /F "tokens=1,2 delims= " %%G IN (%TEMP%\tokensonly.txt) DO cmdkey.exe /delete:%%H
del "%TEMP%\List.txt" /s /f /q
del "%TEMP%\tokensonly.txt" /s /f /q

pause
Goto Choose

:DME_EX
exit
