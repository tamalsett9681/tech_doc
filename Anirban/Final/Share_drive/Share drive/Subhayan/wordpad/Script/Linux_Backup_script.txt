#!/bin/bash
BACKUPDIR=~/backup
HOMEDIR="/home"
BACKUPFILE=scripts.backup.`date +%F`.tar.gz
THRESHOLD=7
if [ ! -e $BACKUPDIR ];
then
echo Creating backup directory it does not exist
mkdir /backup
exit 0
else
COUNT=`ls $BACKUPDIR/home.* | wc -l`
fi
if [ $COUNT -le $THRESHOLD ];
then
tar -czvf $BACKUPDIR/$BACKUPFILE $HOMEDIR
if [ $? != 0 ]; then echo Problems creating backup file; fi
fi