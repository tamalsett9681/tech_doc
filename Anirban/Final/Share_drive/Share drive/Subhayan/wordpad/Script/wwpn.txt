for i in `lsdev -C|grep fcs|cut -d' ' -f1`
do wwn=`lscfg -vpl $i|grep "Network Address"|xargs|cut -d. -f14|sed 's/.\{2\}/&:/g'|sed 's/.$//'`
echo "$i=$wwn"
done