Hello!

My name is Viktor Sebesteny. I'm assisting on the PowerHA problem.

Please send me the PowerHA snap data as follows:

1. Run snap -r on all cluster nodes to remove earlier snap files
2. Run snap -a command on all cluster nodes. This will collect the AIX snap
3. Run snap -e to gather PowerHA snap. Run this command only on one node! The output files are in the /tmp/ibmsupt/hacmp directory.
4. Check the PowerHA snap files: go to /tmp/ibmsupt/hacmp and test all archive files (there should be one archive for each cluster nodes), that you can restore it without any problem. (Recently we encounter lot of cases when the PowerHA snap archive was corrupt).
5. Run snap -c on both nodes. This will put all snap data files into the /tmp/ibmsupt/snap.pax.Z archive. Rename the file, so its name should start with the PMR number and include the hostname as well. For example: XXXXX.YYYY.ZZZZ.node1.snap.pax.Z
6. Upload the file as follows:
ftp ftp.ecurep.ibm.com
LOGIN user: anonymous pw: your_email_address
bin
cd /toibm
cd aix
put XXXXX.YYYY.ZZZZ.*
quit

https://www-01.ibm.com/support/docview.wss?uid=isg3T1000217
http://www-01.ibm.com/support/docview.wss?uid=isg3T1019037