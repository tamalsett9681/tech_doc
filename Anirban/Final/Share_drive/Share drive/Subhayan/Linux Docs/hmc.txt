Activation of an LPAR
chsysstate -r lpar -m pserver -o on -f myprofile -n mylpar

How to Boot an LPAR into SMS Menu
chsysstate -r lpar -m pserver -o on -f myprofile -b sms -n mylpar

The same way the LPAR can be bootet into the Open Firmware prompt (Bootmode: 'of'): 
chsysstate -r lpar -m pserver -o on -f myprofile -b of -n mylpar

 HMC Version
lshmc -V

Network configuration of the HMC
lshmc -n

Reboot the HMC
hmcshutdown -t now -r

How to change the HMC password (of user hscroot)
chhmcusr -u hscroot -t passwd

Show Available Filesystem Space
monhmc -r disk -n 0
