AIX Microcode Update Procedure
Skip to Linux Microcode Update Tool Procedure

PCIe2 LP 4-Port (10Gb+1GbE) SR+RJ45 Adapter (FC EN0T)
PCIe2 LP 4-port (10Gb+1GbE) Copper SFP+RJ45 Adapter (FC EN0V)
PCIe2 4-Port (10Gb+1GbE) SR+RJ45 Adapter (FC EN0S)
PCIe2 4-port (10Gb+1GbE) Copper SFP+RJ45 Adapter (FC EN0U)


******* PLEASE READ THIS ENTIRE NOTICE *********
DATE: June 10, 2019
Table of Contents
1.0 Microcode and Document Revision History
1.5 Important Information
2.0 General information
3.0 Installation time
4.0 Machine's Affected
5.0 AIX Requirements
6.0 Determine the Current Microcode Level for AIX
7.0 Downloading the RPM Format File to the Target Server for AIX
8.0 Discovery Tool Microcode CD-ROM creation and download instructions
9.0 Verifying microcode before download and notes for AIX
10.0 Microcode Download Procedure for AIX
10.1 Setting up for Microcode download
10.2 Downloading Microcode to the Adapter
10.3 Verify and Re-configure adapters.
11.0 Microcode Download Procedure for Systems Without AIX Installed
============================================================
1.0 Microcode and Document Revision History:
Firmware LevelDescription30060010Original Release30080130Boot Support30080130Boot Support- Update build image � Update Checksum value30090140Support added for network installation on 1GB speed switch ports30100150Improved support added for network installation on 1GB speed switch ports30100310Impact: Usability Severity: ATT
Power9-specific NIM install file size fix
Secure Boot support added
Document Revision HistoryDescription05/28/14Creating Readme file with latest microcode for this adapter- 3006001006/28/14Updating Readme file with latest microcode for this adapter - 3008013007/17/14Update Readme file checksum section08/25/14Updating Readme file with latest microcode for this adapter - 303009014009/11/14Clarification of update process for port members of Etherchannel and/or SEA09/30/14Updating Readme file-AIX microcode file name change for newer AIX releases, rpm includes both filenames e4148a169304.30090140 and e4148a1614109304.3009014001/12/15Updating Readme file with latest microcode for this adapter - 3010015011/17/15Updating Readme file with instructions for Little Endian and Big Endian Linux upgrade Tool06/10/19Updating Readme file with latest microcode for this adapter - 301003101.5 Important Information:
Recently, several enhancements were released to improve the reliability and function of new and existing adapters used on Power8 systems. To ensure the highest level of availability and performance, it is important that the following System Firmware, IO, AIX & VIOS maintenance is performed. For efficiency, IBM recommends that all applicable System Firmware, IO, AIX & VIOS maintenance is consolidated and performed during the same session to reduce the number of scheduled maintenance windows. 
* System F/W: SV810_081 / FW810.10 (or higher)
o For systems in PowerVM mode, a problem was fixed for unresponsive PCIe adapters after a partition power off or a partition reboot.
* I/O:
o Device: PCIe2 4-Port (10GbE SFP+ & 1GbE RJ45) Adapter
Feature Codes: EN0S EN0T EN0U EN0V
Version: 30090140 (or higher)
Support added for network installation on 1GB speed switch ports
o Device: PCIe2 2-Port 10GbE Base-T Adapter
Feature Codes: EN0W EN0X
Version: 20110140 (or higher)
Fixes a network installation issue seen with 1GB speed switch port setting.
* AIX/VIOS:
o VIOS 2233/61Z TL09 SP3 : IV63449
o VIOS 2225/61X TL8 SP5 : IV64473
o VIOS 2219/61S TL7 SP10 : IV65041

For Power8 systems using NIC adapter Feature Codes (FC) EN0U, EN0V, EN0S, EN0T, EN0W, EN0X which translate to:
PCIe2 4-Port Adapter (10GbE SFP+)
PCIe2 4-Port Adapter (1GbE RJ45)
PCIe2 2-Port 10GbE Base-T Adapter

These APARs correct a problem that occurs when promiscuous mode is not set when the adapter gets reset (e.g. when adapter becomes backup in SEA fail over mode or Encounters a transmit error). This would cause the adapter to transmit packet but not receive packets.
2.0 General information:
This Readme file is intended to give directions on how to update the microcode found on the PCIe2 4-port (10Gb+1GbE) SFP+RJ45 Adapter (FC EN0T, FC EN0V, FC EN0S, FC EN0U).
3.0 Installation time:
Approximately 20 minutes.
4.0 Machine's Affected:
FC: EN0S/EN0U: 8286-41A or 8286-42A
FC: EN0T/EN0V: 8247-21L, 8247-22L, or 8284-22A
5.0 AIX Requirements:
Basic card function is supported on:
* AIX�
o AIX 7.1, Technology Level 3, Service Pack 2, or later
o AIX 7.1, Technology Level 2, Service Pack 2, or later
o AIX 7.1, Technology Level 1, Service Pack 3, or later
o AIX 6.1, Technology Level 9, Service Pack 2, or later
o AIX 6.1, Technology Level 8, Service Pack 3, or later
o AIX 6.1, Technology Level 7, Service Pack 2, or later
* Linux
o Red Hat Enterprise Linux Version 6.5, or later, with current maintenance updates available from Red Hat.
o SUSE Linux Enterprise Server 11, Service Pack 3, or later, with current maintenance updates available from SuSE.

FIX Central to download microcode:
http://www-933.ibm.com/eserver/support/fixes/fixcentral
To navigate this site, you will want to specify:
������� Product Group: Systems
������� System: Power
������� Product: Firmware, SDMC, and HMC
������� Machine Type: <Enter Machine Type>
������� Select: Device firmware. Obtain device firmware only. Available for adapters hard disks and media devices.
������� Select: By Feature Code
������� Input: EN0S, EN0U, EN0T, or EN0V
������� Download the RPM file
If you are using another release of AIX, ensure that the adapter is supported on that release before you install the adapter. Contact service and support for assistance.
6.0 Determine the Current Microcode Level for AIX:
Before you install the microcode, it is important to determine the microcode level of the Adapter installed in the target system. Use the following instructions to read the ROM level stored in the Adapter's VPD.
1. List the Ethernet adapters installed in the system by typing:
lsdev -C | grep e4148a161
For example:
# lsdev | grep e4148a161
ent4������� Available 03-00������ PCIe2 4-Port Adapter (10GbE SFP+) (e4148a1614109304)
ent5������� Available 03-01������ PCIe2 4-Port Adapter (10GbE SFP+) (e4148a1614109304)
ent6������� Available 03-02������ PCIe2 4-Port Adapter (1GbE RJ45) (e4148a1614109404)
ent7������� Available 03-03������ PCIe2 4-Port Adapter (1GbE RJ45) (e4148a1614109404)

All of the Ethernet adapters that are installed will be displayed. They will be listed as entX, where X is 0, 1, etc.
2. To check the current microcode level for the adapter or controller enter the following command: lscfg -vl entX
Where �X� is the instance of the adapter. The command will produce output similar to:
# lscfg -vl ent10
� ent11 U2C4E.001.DBJ3257-P2-C3-T1 PCIe2 4-Port Adapter (10GbE SFP+) (e4148a1614109304)
����� PCIe2 4-Port (10GbE SFP+ & 1GbE RJ45) Adapter:
������� FRU Number..................00E2715
������� EC Level....................D77452
������� Customer Card ID Number.....2CC3
� ������Part Number.................00E2719
������� Feature Code/Marketing ID...EN0S
������� Serial Number...............YL500838M135
������� Manufacture ID..............40F2E93104D8
������� Network Address.............40F2E93104D8
������� ROM Level.(alterable).......30100310
������� Hardware Location Code......U2C4E.001.DBJ3257-P2-C3-T1
If the ROM Level is less than 30100310 you should update the microcode.
7.0 Downloading the RPM Format File to the Target Server for AIX:
Use this method to download to an AIX system:
NOTE: The instructions that follow are specific AIX commands. AIX commands are CASE (lower and upper) SENSITIVE, and must be entered exactly as shown, including filenames.
1. Make two directories on your AIX system to receive the RPM format file.
Enter:����� �mkdir /tmp/microcode�
and then create this directory
Enter:����� �mkdir /tmp/microcode/RPM�
2. Transfer the RPM format file to the /tmp/microcode/RPM directory (using �Save as ...�).� Change to that directory, �cd /tmp/microcode/RPM�.
You'll see the filename for the RPM file.
��� �� �rpm -ihv --ignoreos� e4148a1614109304.30100310.aix.rpm�
3. For AIX:� The microcode files will be added to /usr/lib/microcode/.
NOTE:
�- �/etc/microcode� is a symbolic link to �/usr/lib/microcode�.
�- If permission does not allow the copy to the above stated directory or file then the user will be prompted for a new location.
8.0 Discovery Tool Microcode CD-ROM creation and download instructions:
To obtain information how to burn a CD-ROM and run the Discovery Tool for an AIX or Linux System please go to:
http://www-304.ibm.com/webapp/set2/firmware/lgjsn?mode=10&page=cdrom.html
1. After running the Discovery Tool successfully the �/tmp/microcode/RPM� directory was created and your rpm files are copied from the CD-ROM.
2. Change to that directory, �cd /tmp/microcode/RPM�.
3. Unpack the file by executing the instructions below:
��� Enter the commands:
������rpm -ihv --ignoreos e4148a1614109304.30100310.aix.rpm
4. Two microcode files will be copied to �/etc/microcode�.� The file size and checksum of the microcode image will be verified in Section 9.0.��������� 
����������� File Name:
����������� e4148a169304.30100310
��������� e4148a1614109304.30100310
NOTE:
- �/etc/microcode� is a symbolic link to �/usr/lib/microcode�.
- If permission does not allow the copy to the above stated directory or file then the user will be prompted for a new location.
- For customers using the AIX Diagnostics CD, please refer to the IBM System Hardware information Center for instructions.
9.0 Verifying microcode before download and notes for AIX:
Please verify the file size and checksum of the raw microcode files matches what is listed below.
Note: Microcode download must be performed separately for each Adapter bus under distinct Logical Partitions (LPAR's). Please save current and older versions of the microcode update files in case you need to restore the system.
Installation Time: �Approximately 30 min System Time.
Please verify the file size and checksum of the raw microcode files matches below:
For AIX :
�ls -l /usr/lib/microcode/e4148a169304.30100310� to verify file size is 2097152.
�sum /usr/lib/microcode/e4148a169304.30100310� to verify Checksum is 35363.
�ls -l /usr/lib/microcode/e4148a1614109304.30100310� to verify file size is 2097152.
�sum /usr/lib/microcode/e4148a1614109304.30100310� to verify Checksum is 35363.
10.0 Microcode Download Procedure for AIX:
Microcode download must be performed separately for each Adapter under each Logical Partitions (LPAR's).
10.1 Setting up for Microcode download:
1. Stop all applications that use this interface/adapter
2. Remove the interface/IP address from the all ports identified in section 6.0 for the adapters that will be upgraded.
a. Before detaching the interface, record the ip address and any other pertinent information that was configured on the Adapter.� This information may be needed if the microcode update overwrites this section on the Adapter.
3. If the interfaces are members of an SEA, the SEA devices must be moved to a defined state.
a. �rmdev -l enX� � where �X� is the interface number for the Shared Ethernet Adapter.
b. �rmdev �l entX� � where �X� is the interface number for the Shared Ethernet Adapter.
4. If the interfaces are members of an EtherChannel, the EtherChannel device must be moved to a define state.
a. �rmdev -l enX� � where �X� is the interface number for the EtherChannel adapter.
b. �rmdev �l entX� � where �X� is the interface number for the EtherChannel adapter.
5. For every port associated with the adapter, the enX interfaces must be changed to a defined state.
a. �rmdev -l enX� - where �X� is the interface number for the adapter port
b. This command will be run 4 times, once for each port on the adapter.
10.2 Downloading Microcode to the Adapter:
1. At the command line type �diag�
2. Select the �Task Selection� from diagnostics menu.
3. Select �Download Microcode� or �Microcode Tasks� then select �Download Microcode� from the menu.
4. Select one of the PCIe2 4-Port Adapter (10GbE SFP+) (e4148a1614109304) devices for each adapter that need to be updated. Select from the list of devices by using the arrow keys to highlight the entry and pressing "Enter" to mark it.� The devices will be displayed as entX, where �X� is the number of the device.� Press �F7� or �ESC+7� when you are done mark all the adapters you want to flash.
5. Select �/usr/lib/microcode� or �/etc/microcode�.
6. A dialogue box may be displayed on screen.� It will state that the current microcode level on the adapter is not in the /usr/lib/microcode directory.� This is acceptable because the adapter will reject any incorrect code.� Press �Enter� to continue.
7. Select 30100310 level and press �Enter� to flash the adapter.
8. The following message will appear on the screen when download is completed: �Microcode download complete successfully.� The current microcode level for the device entX is ...� Please run diagnostics on the adapter to ensure that it is functioning properly.�
9. If you selected more than one adapter to update, then steps 6-9 will repeat until all adapters are updated.
10. Exit diagnostics.
10.3 Verify and Re-configure adapters
1. Verify the code level is 30100310 by typing �lscfg -vl entX� for each Ethernet adapter updated. Where �X� is the instance of the Ethernet adapter
2. Re-Configure the adapter and interfaces by executing the following command: �cfgmgr�
3. Re-configure/Verify the adapter's interface ip information through the �smit� menus.
If the customer runs into VIOS specific issues please contact IBM VIOS support.
NOTE: Once the firmware has been successfully updated, you must use smitty to reconfigure the network.
11.0 Microcode Download Procedure for Systems Without AIX Installed:
Use this procedure if the system has no OS installed or the installed OS (e.g., Linux) does not support microcode download for this device.
1. Boot the system via a Standalone Diagnostic's CD
a. To obtain a Standalone Diagnostic CD-ROM please go to the following webpage and follow the instructions
http://www-304.ibm.com/webapp/set2/sas/f/diags/home.html
b. After booting to Standalone Diagnostics, the Diagnostic's CD can be removed.
2. The Diagnostic's CD should be replaced by a CD containing the unpacked microcode image.

The CD containing the microcode images should be prepared prior. Create a CD-ROM with the firmware e4148a169304.30100310 and e4148a1614109304.30100310 mentioned in section 8.0 and burn the CD with Joliet CD file system.
3. From the Define a System Console menu either type or select vt320.
4. Select the �Task Selection� from diagnostics menu then select �Download Microcode�.
5. Select resource PCIe2 4-Port Adapter (10GbE SFP+) (e4148a1614109304) that microcode will be applied to and PRESS ENTER.
6. Press �F7� or �ESC+7� to commit.(The current microcode level of the resource you selected earlier will be displayed at the top of the screen).
7. Select Input Device (CD-ROM or DVD) then choose the Microcode level to download.

NOTE: A prompt will ask you to insert the microcode CD-ROM into the CD/DVD drive.� Insert the CD-ROM which accompanies these instructions. If this level is already installed in the drive you've selected a message will let you know. A message also may come up to let you know that the CD_ROM does not have the previous level microcode file. This is true and is not required to complete the download. PRESS �F7� or �ESC+7� to commit.
8. You will receive a msg. �Current Microcode is....� and �Download has completed successfully�(this may take a few minutes).
9. Return to the Tasks Selection menu and repeat this procedure for each adapter that requires this microcode.
10. Exit diagnostic and reboot system in normal mode.


Linux Microcode Update Tool Procedure
PCIe2 LP 4-Port (10Gb+1GbE) SR+RJ45 Adapter (FC EN0T)
PCIe2 LP 4-port (10Gb+1GbE) Copper SFP+RJ45 Adapter (FC EN0V)
PCIe2 4-Port (10Gb+1GbE) SR+RJ45 Adapter (FC EN0S)
PCIe2 4-port (10Gb+1GbE) Copper SFP+RJ45 Adapter (FC EN0U)


******* PLEASE READ THIS ENTIRE NOTICE *********
DATE: June 10, 2019
Table of Contents
1.0 Microcode and Document Revision History:
2.0 General information:
3.0 Installation time:
4.0 Machine's Affected:
5.0 Linux Requirements:
6.0 Determine the Current Microcode Level for Linux:
7.0 Downloading the RPM Format File to the Target Server for Linux:
8.0 Verifying microcode before download and notes for Linux:
9.0 Microcode Download Procedure for Linux:
9.1 Setting up for Microcode download:
9.2 Downloading Microcode to the Adapter:
9.3 Verify and Re-configure adapters

============================================================
1.0 Microcode and Document Revision History:
Firmware LevelDescription30060010Original Release30080130Boot Support30080130Boot Support- Update build image � Update Checksum value30090140Support added for network installation on 1GB speed switch ports30100150Improved support added for network installation on 1GB speed switch ports30100310Impact: Usability Severity: ATT
Power9-specific NIM install file size fix
Secure Boot support added
Document Revision HistoryDescription05/28/14Creating Readme file with latest microcode for this adapter- 3006001006/28/14Updating Readme file with latest microcode for this adapter - 3008013007/17/14Update Readme file checksum section08/25/14Updating Readme file with latest microcode for this adapter - 3009014009/11/14Clarification of update process for port members of Etherchannel and/or SEA09/30/14Updating Readme file-AIX microcode file name change for newer AIX releases, rpm includes both filenames e4148a169304.30090140 and e4148a1614109304.3009014001/12/15Updating Readme file with latest microcode for this adapter - 3010015011/17/15Updating Readme file with instructions for Little Endian and Big Endian Linux upgrade Tool06/10/19Updating Readme file with latest microcode for this adapter - 301003102.0 General information:
This Readme file is intended to give directions on how to update the microcode found on the PCIe2 4-port (10Gb+1GbE) SFP+RJ45 Adapter (FC EN0T, FC EN0V, FC EN0S, FC EN0U) in a Little Endian or Big Endian Linux OS.
3.0 Installation time:
Approximately 20 minutes.
4.0 Machine's Affected:
FC: EN0S/EN0U: 8286-41A, 8286-42A, or 8335-GCA
FC: EN0T/EN0V: 8247-21L, 8247-22L, 8284-22A, 9119-MHE, or 8335-GTA
5.0 Linux Requirements:
Basic card function is supported on:
Linux
* Power VM
o Red Hat Enterprise Linux Version 6.5, or later, with current maintenance updates available from Red Hat. 
o SUSE Linux Enterprise Server 11, Service Pack 3, or later, with current maintenance updates available from SuSE.
o RHEL 7.1 LE/7.4 LE/ 7.5-alt LE/7.6-alt LE
o Ubuntu LE 15.04, 16.04.4
o SLES 12 SP3 LE, SLES 15 LE, 
* Bare Metal Linux
o Ubuntu LE 14.04, 14.10, 16.04, 18.04 with current maintenance from Ubuntu
* Power KVM Host
o PowerKVM 2.1
o PowerKVM 3.1 LE
* KVM Guest with PCI passthru
o Red Hat Enterprise Linux Version 6.6, or later, with current maintenance updates available from Red Hat.
o SUSE Linux Enterprise Server 11, Service Pack 3, or later, with current maintenance updates available from SuSE.
o Ubuntu LE 14.04, Ubuntu LE 14.10, Ubuntu 16.04
o SUSE Linux Enterprise Server 12 LE
o RHEL 7.1 LE

FIX Central to download microcode:
http://www-933.ibm.com/eserver/support/fixes/fixcentral
To navigate this site, you will want to specify:
������� Product Group: Systems
������� System: Power
������� Product: Firmware, SDMC, and HMC
������� Machine Type: <Enter Machine Type>
������� Select: Device firmware. Obtain device firmware only. Available for adapters hard disks and media devices.
������� Select: By Feature Code
������� Input: EN0S, EN0U, EN0T, or EN0V
������� Download the RPM file
If you are using another release of Linux, ensure that the adapter is supported on that release before you install the adapter. Contact service and support for assistance.

Linux Tool for PPC
Download lnxfwnx2 Tool from the vendor website and copy it to your linux OS: http://driverdownloads.qlogic.com/QLogicDriverDownloads_UI/IBM_Search.aspx
Intelligent Ethernet Adapters: BCM957800A00006ICDM : Management Tools for IBM Power Systems <go>
If you have a Little Endian Linux OS on PowerPC select: FW Update Tool for Linux LE
If you have a Big Endian Linux OS on PowerPC select: FW Update Tool for Linux BE
Latest tool lnxfwnx2 tool v2.10.72

Installation:
================

     1.  To install lnxfwnx2 SDK package.

         	a.  Please do 'tar -zxvf lnxfwnx2-{arch}.sdk.tgz' to untar the
             package.

             The files included in the sdk package are:
             1)  lnxfwnx2                	-  The upgrade utility program
             2)  libtcl8.6.8.so	 	-  Dependent tcl8.6.8 library
             3)  Readme.txt              	-  The Readme file.
             4)  Release.txt
             5)  qlgc_pci.ids
6.0 Determine the Current Microcode Level for Linux:
Before you install the microcode, it is important to determine the microcode level of the Adapter installed in the target system. Use the following instructions to read the USR_BLK and FCODE level stored in the Adapter's VPD.
NOTE: Be sure to be logged in with root user access.
1.	List the Ethernet adapters installed in the system by typing:
lspci |grep "NetXtreme II BCM57800"
For example:
lspci |grep "NetXtreme II BCM57800"
0003:0f:00.0 Ethernet controller: Broadcom Corporation NetXtreme II BCM57800 1/10 Gigabit Ethernet (rev 10)
0003:0f:00.1 Ethernet controller: Broadcom Corporation NetXtreme II BCM57800 1/10 Gigabit Ethernet (rev 10)
0003:0f:00.2 Ethernet controller: Broadcom Corporation NetXtreme II BCM57800 1/10 Gigabit Ethernet (rev 10)
0003:0f:00.3 Ethernet controller: Broadcom Corporation NetXtreme II BCM57800 1/10 Gigabit Ethernet (rev 10)

In addition use ls -la /sys/class/net |grep <bus-id> to list the corresponding network device names
ls -la /sys/class/net |grep 0003:0f:00
lrwxrwxrwx  1 root root 0 Sep 14 14:42 eth14 -> ../../devices/pci0003:00/0003:00:00.0/0003:01:00.0/0003:02:11.0/0003:0f:00.2/net/eth14
lrwxrwxrwx  1 root root 0 Sep 14 14:42 eth15 -> ../../devices/pci0003:00/0003:00:00.0/0003:01:00.0/0003:02:11.0/0003:0f:00.0/net/eth15
lrwxrwxrwx  1 root root 0 Sep 14 14:42 eth16 -> ../../devices/pci0003:00/0003:00:00.0/0003:01:00.0/0003:02:11.0/0003:0f:00.3/net/eth16
lrwxrwxrwx  1 root root 0 Sep 14 14:42 eth17 -> ../../devices/pci0003:00/0003:00:00.0/0003:01:00.0/0003:02:11.0/0003:0f:00.1/net/eth17

All of the Ethernet adapters that are installed will be displayed. They will be most likely be listed as ethX, where X is 0, 1, etc.

2.	To check the current microcode and query the FCODE and USR_BLK level for the adapter or controller use the lnxfwnx2 tool command
Where �X� is the instance of the adapter. The command input and output will be similar to:
./lnxfwnx2 ethX dir |grep -E 'FCODE|USR_BLK' |awk '{print $NF}' |sort -r
3010
0310

This corresponds to FW level 30100310.
If the ROM Level is less than 30100310 you should update the microcode.
7.0 Downloading the RPM Format File to the Target Server for Linux:
Use this method to download to a Linux system:
NOTE: The instructions that follow are specific Linux commands. Linux commands are CASE (lower and upper) SENSITIVE, and must be entered exactly as shown, including filenames.
4. Make two directories on your Linux system to receive the RPM format file.
Enter:����� �mkdir /tmp/microcode�
and then create this directory
Enter:����� �mkdir /tmp/microcode/RPM�
5. Transfer the RPM format file to the /tmp/microcode/RPM directory (using �Save as ...�).� Change to that directory, �cd /tmp/microcode/RPM�.
You'll see the filename for the RPM file.
��� �� �rpm -ihv --ignoreos� e4148a1614109304.30100310.Linux.rpm�
6. For Linux:� The microcode files will be added to /lib/firmware/ directory.
8.0 Verifying microcode before download and notes for Linux:
Please verify the file size and checksum of the raw microcode files matches what is listed below.
Note: Microcode download must be performed separately for each Adapter and can only be done from the Host OS. Please save current and older versions of the microcode update files in case you need to restore the system.
Installation Time: �Approximately 20 min System Time.
Please verify the file size and checksum of the raw microcode files matches below:
For Linux :
�ls -l /usr/lib/microcode/e4148a169304.30100310� to verify file size is 2097152.
�sum /usr/lib/microcode/e4148a169304.30100310� to verify Checksum is 35363.
�ls -l /usr/lib/microcode/e4148a1614109304.30100310� to verify file size is 2097152.
�sum /usr/lib/microcode/e4148a1614109304.30100310� to verify Checksum is 35363.
9.0 Microcode Download Procedure for Linux:
Microcode download must be performed separately for each Adapter.
Performing microcode download on one port of the adapter is enough to update the whole card.
9.1 Setting up for Microcode download:
6. Stop all applications that use this interface/adapter
7. Remove the interface/IP address from the all ports identified in section 6.0 for the adapters that will be upgraded.
a. Before detaching the interface, record the ip address and any other pertinent information that was configured on the Adapter.� This information may be needed if the microcode update overwrites this section on the Adapter.
8. If the interfaces are members of an etherchannel bond you must first remove it.
9.2 Downloading Microcode to the Adapter:
11. At the command line run ifconfig up for all adapter port interfaces.
12. ./lnxfwnx2 eth14 restorenvram /lib/firmware/e4148a1614109304.30100310 preserve vpd
13. Repeat for any other "NetXtreme II BCM57800" adapter.
14. Reboot in order to apply the firmware update.
9.3 Verify and Re-configure adapters
4. Verify the code level is 30100310:
To check the current microcode and query the FCODE and USR_BLK level for the adapter or controller use the lnxfwnx2 tool command
Where �X� is the instance of the adapter. The command input and output will be similar to:
./lnxfwnx2 ethX dir |grep -E 'FCODE|USR_BLK' |awk '{print $NF}' |sort �r
3010
0310 
5. Re-Configure the adapter and interfaces
6. Re-configure/Verify the adapter's interface ip information
