ACL for NFS file system

aclget /sapftp/UX1/XI/PaymentFI

aclconvert -R -t NFS4 /sapftp/UX1/XI/PaymentFI   #########   if file system is v2 then it will ACL_type   AIXC  #######

chfs -a ea=v2 /sapftp/UX1/XI/PaymentFI   if the file system property not V2
aclconvert -R -t NFS4 /sapftp/UX1/XI/PaymentFI
ls -l /sapftp/UX1/XI/PaymentFI
export EDITOR=`which vi`   or export EDITOR=/usr/bin/vi
acledit /sapftp/UX1/XI/PaymentFI
id ftp_idc
acledit /sapftp/UX1/XI/PaymentFI
acledit /sapftp/UX1/XI/PaymentFI/OUT
acledit /sapftp/UX1/XI/PaymentFI/ARCHIVE
cd /sapftp/UX1/XI/PaymentFI
ls
 

s:(OWNER@):     a       rwpRWxDaAdcCo   fidi
s:(GROUP@):     a       rwpxDad         fidi
s:(OWNER@):     d       rwpRWxDdos      fidi
s:(GROUP@):     d       rwpRWxDaAdcCos  fidi
s:(EVERYONE@):  a       rwpRxDadc       fidi

aclget /sapftp|aclput -t NFS4 -R /sapftp  ## Recursively replicate the ACL of /sapftp to directories under /sapftp

example

aclget /sapftp/NPS/PR1/100/From_MOVEIT/Incoming_files/Incoming_Bank_statement/DB|aclput -t NFS4 -R /sapftp/NPS/PR1/100/From_MOVEIT/Incoming_files/Incoming_Bank_statement/DB/Incoming_files

aclget /sapftp/NPS/PR1/100/From_MOVEIT/Incoming_files/Incoming_Bank_statement/DB|aclput -t NFS4 -R /sapftp/NPS/PR1/100/From_MOVEIT/Incoming_files/Incoming_Bank_statement/DB/Errors
