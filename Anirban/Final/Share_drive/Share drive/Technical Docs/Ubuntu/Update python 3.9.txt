export http_proxy=http://cph2.sme.zscloud.net:80
export https_proxy=http://cph2.sme.zscloud.net:80
export HTTP_PROXY=http://cph2.sme.zscloud.net:80
export HTTPS_PROXY=http://cph2.sme.zscloud.net:80
wget -q -O /usr/local/share/ca-certificates/ZscalerRootCertificate-2048-SHA256.crt http://10.48.64.61/ZscalerRootCertificate-2048-SHA256.crt --no-proxy
update-ca-certificates
add-apt-repository -y 'ppa:deadsnakes/ppa'
apt-get update
apt-get install python3.9 python3.9-distutils -y
update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.6 1
update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.9 2
update-alternatives --config python3
cd /usr/lib/python3/dist-packages
cp apt_pkg.so apt_pkg.so.bkp
cp apt_pkg.cpython-36m-x86_64-linux-gnu.so apt_pkg.so
ln -s /usr/lib/python3/dist-packages/gi/_gi.cpython-36m-x86_64-linux-gnu.so /usr/lib/python3/dist-packages/gi/_gi.cpython-39-x86_64-linux-gnu.so
sed -i 's/isAlive()/is_alive()/g' /usr/lib/python3/dist-packages/softwareproperties/SoftwareProperties.py 
add-apt-repository -r 'ppa:deadsnakes/ppa'
apt-get update
