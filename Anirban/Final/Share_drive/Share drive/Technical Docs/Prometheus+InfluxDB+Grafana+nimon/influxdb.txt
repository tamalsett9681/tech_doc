===========================
Command to invoke influxdb:
===========================
# influx

====================================================
Database creation with retention policy of 52 weeks:
====================================================
> create database <DB Name> with duration 52w replication 1

====================================
User creation and assign privileges:
====================================
> create user <username> with password '<password>' with all privileges

====================
Grant access to DB:
====================
> grant all on "<DB Name>" to "<username>"

==============
List Databes:
==============
> show databases

==============
List Users:
==============
> show users

======================
Connect to a Database:
======================
> connect <DB Name>

===============================
Show user's database privilege:
===============================
> show grants for <username>

=======================
Reset user's password:
=======================
> set password for <username> = '<New password>'

=======================
Remove access for user:
=======================
> revoke [read,write,all] on <DB Name> from <username>

==============
Remove a user:
==============
> drop user <username>

===================
Remove a database:
===================
> drop database <DB Name>

==========================
Create Retention policy:
==========================
> create retention policy <policy name> on <DB name> replication 1 [default]

=========================
Modify retention policy:
=========================
> alter retention policy <policy name> on <DB name> duration 52w replication 1 [default]

=========================
Delete retention policy:
=========================
> drop retention policy <policy name> on <DB name>

==========================
Check retention policies:
==========================
> show retention policies on <DB name>


