
fscsi attribute modification:

chdev -dev fscsi0 -attr fc_err_recov=fast_fail dyntrk=yes

Changing the fc_err_recov attribute to fast_fail fails any I/Os immediately if
the adapter detects a link event, such as a lost link between a storage device and
a switch. The fast_fail setting is only recommended for dual Virtual I/O Server
configurations. Setting the dyntrk attribute to yes allows the Virtual I/O Server to
tolerate cable changes in the SAN.


Fibre Channel device driver attributes


num_cmd_elems modifies the number of commands that can be queued to the
adapter.
max_xfer_size has an effect on the direct memory access (DMA) region
size that is used by the adapter.Default max_xfer_size (0x100000) gives a small DMA region size.Tuning up max_xfer_size to 0x200000 provides a medium or large DMA region size, depending on the adapter.

$ chdev -dev fcs0 -attr num_cmd_elems=2048 max_xfer_size=0x200000 -perm
fcs0 changed
$ chdev -dev fcs0 -attr max_xfer_size=0x200000 -perm
fcs0 changed


Fscsi and fcs changes: Any change to the fscsi and fcs attributes need to be
first checked with your storage vendor.


chdev -dev fcs2 -attr max_xfer_size=0x200000 -perm


=============
Client LPARs:
=============

chdev -l fcs2 -a num_cmd_elems=512 -a max_xfer_size=0x200000 -P