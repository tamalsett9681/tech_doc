#!/bin/ksh
# This Script will print all the details of AIX system and capture all the output in /tmp
# Should be run from root

HNAME=`hostname`\_`date '+%d-%m-%Y_%H%M'`

touch /tmp/$HNAME.out

echo "HOSTNAME" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
hostname >>/tmp/$HNAME.out
uname -Muna >>/tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "Date" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
date >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "Time Zone" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
echo $TZ >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "oslevel -s" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
oslevel -s >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "lsmcode -c" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
lsmcode -c >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "PAGING" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
lsps -a >>/tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "ifconfig -a" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
ifconfig -a >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "NETSTAT -NR" >> /tmp/$HNAME.out
netstat -rn >>/tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo " cat /etc/hosts" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
cat /etc/hosts >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "bootinfo -r" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
bootinfo -r >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "lppchk -v" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
lppchk -v >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "lsdev -Cc disk" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
lsdev -Cc disk >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "lspv" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
lspv >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
for i in `lspv |awk '{print $1}'`
do
echo "size of::" $i >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
bootinfo -s $i >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
echo "lspv" $i >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
lspv $i >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
echo "lspv -l" $i >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
lspv -l $i >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
echo "lsattr -El" $i >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
lsattr -El $i >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
echo "lscfg -vl" $i >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
lscfg -vl $i >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
done
echo "**************************************************" >> /tmp/$HNAME.out
echo "PRTCONF" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
prtconf >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "df -gt" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
df -gt >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "mount" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
mount >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "lsvg" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
lsvg >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
for i in `lsvg -o`
do
        echo "---------------------------------------" >> /tmp/$HNAME.out
        echo "lsvg" $i >> /tmp/$HNAME.out
        echo "---------------------------------------" >> /tmp/$HNAME.out
        lsvg $i >> /tmp/$HNAME.out
        echo "---------------------------------------" >> /tmp/$HNAME.out
        echo "lsvg -l" $i >> /tmp/$HNAME.out
        echo "---------------------------------------" >> /tmp/$HNAME.out
        lsvg -l $i >> /tmp/$HNAME.out
        echo "---------------------------------------" >> /tmp/$HNAME.out
        echo "ls -al /dev/"$i >> /tmp/$HNAME.out
        echo "---------------------------------------" >> /tmp/$HNAME.out
        ls -al /dev/$i >> /tmp/$HNAME.out
	echo "---------------------------------------" >> /tmp/$HNAME.out
	echo "lsvg -P" $i >> /tmp/$HNAME.out
	echo "---------------------------------------" >> /tmp/$HNAME.out
	lsvg -P $i >> /tmp/$HNAME.out
	echo "---------------------------------------" >> /tmp/$HNAME.out
	echo "lsmp -A" $i >> /tmp/$HNAME.out
	echo "---------------------------------------" >> /tmp/$HNAME.out
	lsmp -A $i >> /tmp/$HNAME.out	
	echo "---------------------------------------" >> /tmp/$HNAME.out
done
echo "**************************************************" >> /tmp/$HNAME.out
echo "lparstat -i" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
/usr/bin/lparstat -i >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo " cat /etc/filesystems" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
cat /etc/filesystems >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo " cat /etc/passwd" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
cat /etc/passwd >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo " cat /etc/group" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
cat /etc/group >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo " cat /etc/resolv.conf" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
cat /etc/resolv.conf >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo " SHOWMOUNT" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
showmount -e >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo " CRONTAB" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
crontab -l >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "INITTAB" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
cat /etc/inittab >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo " PRINTER" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
lpstat -sl >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
for i in `lsfs | tail +2 |awk '{print $3}'`
do
echo "ls -ld" $i >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
ls -ld $i >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
done
echo " PowerHA version" >> /tmp/$HNAME.out
echo "---------------------------------------" >> /tmp/$HNAME.out
lslpp -l |grep cluster >> /tmp/$HNAME.out
echo "**************************************************" >> /tmp/$HNAME.out
echo "!!!!!!!ENJOY SCRIPT COMPLETED SUCCESSFULLY!!!!!!!" >> /tmp/$HNAME.out
