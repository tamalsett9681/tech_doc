[Unit]
Description=Broker Service
After=network.target
Wants=kafka.service
Before=kafka.service

[Service]
User=root
TimeoutStartSec=0
Type=simple
KillMode=control-group
WorkingDirectory=/data/kafka
ExecStart=/data/kafka/broker.sh
Restart=always
RestartSec=2
LimitNOFILE=5555

[Install]
WantedBy=default.target
RequiredBy=kafka.service
Also=kafka.service
