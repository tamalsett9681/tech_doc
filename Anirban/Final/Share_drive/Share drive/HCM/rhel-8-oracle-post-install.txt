#!/bin/sh

HOST=`hostname`
LOGFILE=/tmp/post-provisioning-`date '+%d%m%Y'`.log

# Disable Firewall
echo "=======================================" > $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Disabling Firewall and Selinux ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
systemctl stop firewalld >> $LOGFILE 2>&1
systemctl disable firewalld >> $LOGFILE 2>&1
sed -i '7s/.*/SELINUX=disabled/' /etc/selinux/config
setenforce 0 >> $LOGFILE 2>&1

#creating local users, which exists
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Creating Admin User ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
groupadd srv_acct >> $LOGFILE 2>&1
useradd -u 1101 -g srv_acct -c "TCS System Admin" -p '$6$r2UWxo/bw4iCMjY5$s5vxwh3P15mvdCyjlZjxpP83OP2RgU5NWg.EVIBAEzIDGhJ.fbBz91lTMzNz0PpvzGwgzuCHbl4pwG29xZBA8.' tcsuser >> $LOGFILE 2>&1
useradd -u 1102 -g srv_acct -c "Backup User" -p '$6$TNmcbsE8I5r3qO.L$4yf5fBUZ9VAsux3gLCjmOJs5lVBAS9eGuwk8vZsiJXi1cxJDQ.w5NLlK3ifgjj8wO26ODcrp/MjwxueXFq42B.' bkpadmin  >> $LOGFILE 2>&1
useradd -u 1103 -c "Device42 User" -p '$6$FoQy.rmwP25JdfTG$Da5CTNQIMfArWc8eOf2VxBG.ZHrkPM07AYOwQA3.D821BuC07.rNO2DOlfOOu82/NaljqH5YAuISfl9PIwwm40' device42  >> $LOGFILE 2>&1
usermod -p '$6$uh0BnWDPOEUaMiRH$eEkiTD.VnqFgYyaJi3s8IsmNdD99OJNsIdOZ5LQ/vhIw3Vog.18CD1x1iJ2KqIwRjR4Z1wnD/sTkAVVa3hOnx0' root

#setting admin access
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Providing sudo access to Admin User ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/echo "%srv_acct ALL=(ALL) ALL" >> /etc/sudoers

#setting timezone
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Setting Timezone ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/usr/bin/rm -rf /etc/localtime >> $LOGFILE 2>&1
/usr/bin/ln -s /usr/share/zoneinfo/Europe/Copenhagen /etc/localtime >> $LOGFILE 2>&1

#Installing yum packages
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Installing required Packages ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/sed -i 's/proxy_hostname =.*/proxy_hostname = tcsproxy.prd1.prdroot.net/g' /etc/rhsm/rhsm.conf
/bin/sed -i 's/proxy_port =.*/proxy_port = 8080/g' /etc/rhsm/rhsm.conf
/bin/sed -i 's/skip_if_unavailable=False/skip_if_unavailable=True/g' /etc/dnf/dnf.conf
/bin/echo "[baseos]" > /etc/yum.repos.d/local.repo
/bin/echo "name=local-baseos" >> /etc/yum.repos.d/local.repo
/bin/echo "baseurl=http://10.48.75.114/rhel-8-server-rpms/rhel-8-for-x86_64-baseos-rpms" >> /etc/yum.repos.d/local.repo
/bin/echo "gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release" >> /etc/yum.repos.d/local.repo
/bin/echo "enabled=1" >> /etc/yum.repos.d/local.repo
/bin/echo "gpgcheck=1" >> /etc/yum.repos.d/local.repo
/bin/echo -e "proxy=_none_\n" >> /etc/yum.repos.d/local.repo
/bin/echo "[appstream]" >> /etc/yum.repos.d/local.repo
/bin/echo "name=local-appstream" >> /etc/yum.repos.d/local.repo
/bin/echo "baseurl=http://10.48.75.114/rhel-8-server-rpms/rhel-8-for-x86_64-appstream-rpms" >> /etc/yum.repos.d/local.repo
/bin/echo "gpgkey=file:///etc/pki/rpm-gpg/RPM-GPG-KEY-redhat-release" >> /etc/yum.repos.d/local.repo
/bin/echo "enabled=1" >> /etc/yum.repos.d/local.repo
/bin/echo "gpgcheck=1" >> /etc/yum.repos.d/local.repo
/bin/echo "proxy=_none_" >> /etc/yum.repos.d/local.repo
/bin/dnf makecache >> $LOGFILE 2>&1
dnf install sssd realmd oddjob oddjob-mkhomedir adcli samba-common samba-common-tools krb5-workstation chrony wget unzip nmap telnet nfs-utils net-tools bind-utils psmisc cockpit -y >> $LOGFILE 2>&1
systemctl enable --now cockpit.socket >> $LOGFILE 2>&1
systemctl disable --now kdump >> $LOGFILE 2>&1

# NTP Configuration
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Configuring NTP/Chrony ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/sed -i '3s/.*/#&/' /etc/chrony.conf
/bin/echo "server 10.59.229.242 iburst" >> /etc/chrony.conf 
/bin/echo "server 10.48.229.242 iburst" >> /etc/chrony.conf
/usr/bin/systemctl enable chronyd >> $LOGFILE 2>&1
/usr/bin/systemctl restart chronyd >> $LOGFILE 2>&1
/bin/chronyc makestep >> $LOGFILE 2>&1

# changing password aging
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Setting Password Aging ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/sed -i 's/PASS_MAX_DAYS[[:space:]]99999/PASS_MAX_DAYS   180/g' /etc/login.defs
/bin/sed -i 's/PASS_MIN_LEN[[:space:]]5/PASS_MIN_LEN    8/g' /etc/login.defs
/bin/sed -i 's/PASS_WARN_AGE[[:space:]]7/PASS_WARN_AGE   28/g' /etc/login.defs

# setting password complexity
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Setting Password Complexity ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/sed -i '6s/.*/auth            required        pam_wheel.so root_only use_uid debug/' /etc/pam.d/su
/usr/bin/systemctl enable --now oddjobd >> $LOGFILE 2>&1
authselect create-profile password-policy -b sssd >> $LOGFILE 2>&1
authselect select custom/password-policy --force >> $LOGFILE 2>&1
authselect current >> $LOGFILE 2>&1
authselect enable-feature with-mkhomedir >> $LOGFILE 2>&1
authselect enable-feature with-faillock >> $LOGFILE 2>&1
echo "deny=5" >> /etc/security/faillock.conf
echo "unlock_time=1800" >> /etc/security/faillock.conf
sed -i '/pam_pwquality.so/a password    requisite                                    pam_pwhistory.so remember=5 use_authtok' /etc/authselect/custom/password-policy/password-auth
sed -i '/pam_pwquality.so/a password    requisite                                    pam_pwhistory.so remember=5 use_authtok' /etc/authselect/custom/password-policy/system-auth
echo -e "minlen = 9\nminclass = 3\nmaxrepeat = 3\nmaxclassrepeat = 3\nlcredit = -1\nucredit = -1\ndcredit = -1\nocredit = -1\ndifok = 5\necoscheck = 1" >> /etc/security/pwquality.conf
/bin/sed -i '81i\Defaults    logfile = /var/log/sudo.log' /etc/sudoers
echo "session     required                                     pam_tty_audit.so enable=*" >> /etc/authselect/custom/password-policy/password-auth
echo "session     required                                     pam_tty_audit.so enable=*" >> /etc/authselect/custom/password-policy/system-auth
authselect apply-changes >> $LOGFILE 2>&1

# changing ssh configuration
echo -e "\n\n=====================================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Modifying SSH configuration with command history ..." >> $LOGFILE
echo "=====================================================" >> $LOGFILE
/bin/sed -i 's/PermitRootLogin yes/PermitRootLogin no/' /etc/ssh/sshd_config
/bin/sed -i 's/#ClientAliveInterval 0/ClientAliveInterval 3600/' /etc/ssh/sshd_config
/bin/sed -i 's/#ClientAliveCountMax 3/ClientAliveCountMax 0/' /etc/ssh/sshd_config
/bin/sed -i 's/GSSAPIAuthentication yes/GSSAPIAuthentication no/' /etc/ssh/sshd_config
/bin/systemctl restart sshd >> $LOGFILE 2>&1
/bin/sed -i '12s/.*/max_log_file = 256/' /etc/audit/auditd.conf
/bin/sed -i '13s/.*/num_logs = 7/' /etc/audit/auditd.conf
/usr/bin/systemctl enable --now auditd >> $LOGFILE 2>&1
/usr/sbin/service auditd restart >> $LOGFILE 2>&1
/bin/sed -i 's/HISTSIZE=1000/HISTSIZE=5000/g' /etc/profile
/bin/echo "shopt -s cmdhist" >> /etc/bashrc
/bin/echo 'export HISTTIMEFORMAT="%d-%m-%Y %T "' >> /etc/bashrc
/bin/echo 'export HISTFILESIZE=5000' >> /etc/bashrc
/bin/echo 'export PROMPT_COMMAND="${PROMPT_COMMAND:+$PROMPT_COMMAND$"\n"}history -a; history -c; history -r"' >> /etc/bashrc

# zscaler part
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Installing Zscaler Certificate ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/wget -q -O /etc/pki/ca-trust/source/anchors/ZscalerRootCertificate-2048-SHA256.crt http://10.48.64.61/ZscalerRootCertificate-2048-SHA256.crt >> $LOGFILE 2>&1
/bin/update-ca-trust enable >> $LOGFILE 2>&1
/bin/update-ca-trust extract >> $LOGFILE 2>&1

# IPV6 disabling
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Disabling IPv6 ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/echo "net.ipv6.conf.all.disable_ipv6 = 1" >> /etc/sysctl.conf
/bin/echo "net.ipv6.conf.default.disable_ipv6 = 1" >> /etc/sysctl.conf
/sbin/sysctl -p >> $LOGFILE 2>&1

# flexera & bigfix agent installation
echo -e "\n\n============================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Installing Flexera/BigFix/McAfee agents ..." >> $LOGFILE
echo "============================================" >> $LOGFILE
/bin/wget -q -O /home/tcsuser/flexera.sh http://10.48.64.61/flexera.sh
/bin/sh /home/tcsuser/flexera.sh >> $LOGFILE 2>&1
/bin/wget -q -O /home/tcsuser/bigfix.sh http://10.48.64.61/bigfix.sh
/bin/sh /home/tcsuser/bigfix.sh >> $LOGFILE 2>&1
/bin/wget -q -O /home/tcsuser/qualys.sh http://10.48.64.61/qualys.sh
/bin/sh /home/tcsuser/qualys.sh >> $LOGFILE 2>&1

# RHEL Subscription & McAfee agent installation
echo -e "\n\n============================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Subscribing to RHSM ..." >> $LOGFILE
echo "============================================" >> $LOGFILE
/usr/bin/dnf install python38 >> $LOGFILE 2>&1
/bin/wget -q -O /home/tcsuser/mcafee.sh http://10.48.64.61/mcafee.sh
/bin/sh /home/tcsuser/mcafee.sh >> $LOGFILE 2>&1

# Domain joinin part dns entry 
echo -e "\n\n=======================================" >> $LOGFILE
echo "Time: `date '+%d-%m-%Y %H:%M:%S'` Domain Joining and DNS Entry ..." >> $LOGFILE
echo "=======================================" >> $LOGFILE
/bin/wget -q -O /home/tcsuser/domainjoin.sh http://10.48.64.61/domainjoin.sh
/bin/sh /home/tcsuser/domainjoin.sh >> $LOGFILE 2>&1
groupadd admin
realm deny --all >> $LOGFILE 2>&1
realm permit tatatas >> $LOGFILE 2>&1
realm permit tatasds >> $LOGFILE 2>&1
realm permit tataana >> $LOGFILE 2>&1
realm permit mcafee.admin >> $LOGFILE 2>&1
realm permit sa01.ignio >> $LOGFILE 2>&1
realm permit sa01.ECPSolarwinds >> $LOGFILE 2>&1
/bin/systemctl reset-failed sssd >> $LOGFILE 2>&1
systemctl restart sssd >> $LOGFILE 2>&1
/bin/echo "%admin ALL=(ALL) ALL" > /etc/sudoers.d/admin
/bin/echo "sa01.ECPSolarwinds ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/solarwinds
/bin/echo "sa01.ignio ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ignio
/bin/echo "mcafee.admin ALL=(ALL) ALL" > /etc/sudoers.d/mcafee
usermod -a -G admin tatatas >> $LOGFILE 2>&1
usermod -a -G admin tatasds >> $LOGFILE 2>&1
usermod -a -G admin tataana >> $LOGFILE 2>&1
dnf update -y >> $LOGFILE 2>&1
/bin/systemctl reset-failed sssd >> $LOGFILE 2>&1
systemctl restart sssd >> $LOGFILE 2>&1
/usr/bin/authselect select custom/password-policy --force  >> $LOGFILE 2>&1
/usr/bin/authselect enable-feature with-mkhomedir >> $LOGFILE 2>&1
/usr/bin/authselect enable-feature with-faillock >> $LOGFILE 2>&1
/usr/bin/authselect apply-changes >> $LOGFILE 2>&1
/usr/bin/authselect current >> $LOGFILE 2>&1