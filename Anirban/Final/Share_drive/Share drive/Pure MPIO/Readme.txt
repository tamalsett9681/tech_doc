==============
How to install
==============
# installp -acYd. devices.fcp.disk.pure.flasharray.mpio.rte
Then reboot

=============
VIOS Settings
=============
# chdev -l fcs0 -a max_xfer_size=0x400000 -P

============
AIX Settings
============
1. Zone only one HBA port on AIX to the desired Pure controller port(s).
2. You will only have one hdisk to install on to.
3. Install AIX on hdisk.
4. You will see
	# lsdev -Cc disk
	hdisk0 Available 00-00-00 SAS Disk Drive
	hdisk1 Available 00-00-00 SAS Disk Drive
	hdisk2 Available 05-00-01 Other FC SCSI Disk Drive
5. You can then install the PURE ODM definition. 
6. Reboot, as prompted by the tool.
7. PURE will have replaced the Other FC drive
	# lsdev -Cc disk
	hdisk0 Available 00-00-00 SAS Disk Drive
	hdisk1 Available 00-00-00 SAS Disk Drive
	hdisk2 Available 05-00-01 PURE MPIO Drive (Fibre)
8. It is possible a SCSI Reservation still exists on the Pure array.  Versions prior to AIX 6.1TL7 will need to clear these reservations using their preferred method.
For AIX 6.1TL7+ (Including 7.1 and 7.2) run the following to clear the reservation from AIX before proceeding: 
	# devrsrv -f -c release -l hdisk2
	Device Reservation State Information
	==================================================
	Device Name : hdisk2
	Device Open On Current Host? : YES
	ODM Reservation Policy : NO RESERVE
	Device Reservation State : NO RESERVE
	Device is currently Open on this host by a process.
	Do you want to continue y/n:y
	The command was successful.
Note: There is an IBM issue where the "devrsrv -f -c release -l hdisk1" command fails to clear SCSI-2 reservation see IBM's IV76821 for 6.1 and IV76995 for 7.1.
9. You can now continue adding your subsequent zoned paths and LUN's.

=========================
Multipath Recommendations
=========================
# chdev -l hdiskX -a algorithm=shortest_queue

================
Check the LUN ID
================
# odmget -q "name=hdisk0 and attribute=unique_id" CuAt
CuAt:
 name = "hdisk2"
 attribute = "unique_id"
 value = "3A213624A937018563310400CACD7000115F10AFlashArray04PUREfcp"
 type = "R"
 generic = ""
 rep = "nl"
 nls_index = 79

============
AIX Settings
============
Fast Fail and Dynamic Tracking should be enabled for any HBA port zoned to a Pure FlashArray port. 
	# chdev -l fscsi0 -a fc_err_recov=fast_fail -P 
	fscsi0 changed 
	# chdev -l fscsi0 -a dyntrk=yes -P 
	fscsi0 changed 
	# chdev -l fcs0 -a max_xfer_size=0x200000 -P 
	fcs0 changed
