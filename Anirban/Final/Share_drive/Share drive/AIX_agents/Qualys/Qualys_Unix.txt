AIX (.bff.gz) Installation Requirements
•	Click here for the list of supported operation system versions.
•	Your host must be able to reach the Qualys Cloud Platform or the Qualys Private Cloud Platform over HTTPS port 443.
•	To install the agent you must have 1) root privileges, 2) non-root with Sudo root delegation, or 3) non-root with sufficient privileges (VM only).
	
Steps to Install the AIX Agent

•	Download the agent installer (file size 12.1 MB) (Present at \\10.48.75.26\d$\For Unix Linux)
File will be saved to your downloads area, as defined by your local system.
•	Copy qualys-cloud-agent.aix_power_64.bff.gz to the host you want to monitor and run commands


sudo gzip -fd qualys-cloud-agent.aix_power_64.bff.gz >qualys-cloud-agent.aix_power_64.bff 
sudo installp -acXd . qualys-cloud-agent.rte
sudo /opt/qualys/cloud-agent/bin/qualys-cloud-agent.sh ActivationId=dca91a9f-6266-464b-a9de-3ce35a1e6620 CustomerId=b72f064a-719e-516e-812f-8b9e887ee12e
