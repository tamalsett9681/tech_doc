•        Stop agent service  
lssrc -s swiagentd
stopsrc -s swiagentd

•        Check for running agent services such as JobEngine, agent, agent update
ps -ef|grep -i solarwind

•        If there are agent services which are still running even after stopping the agent, you would need to kill these services. Ensure the update.service is killed as well. 
o   kill -9 <PID>


•        Start agent service
lssrc -s swiagentd
startsrc -s swiagentd


•        Check services that are running after. These services should all be newly spawned and the update.service should not start running. If they are still running, 
Run these one command at a time
 
/opt/SolarWinds/Agent/bin/swiagentaid.sh enable swiagentd.update
/opt/SolarWinds/Agent/bin/swiagentaid.sh stop swiagentd.update
/opt/SolarWinds/Agent/bin/swiagentaid.sh disable swiagentd.update

ps -ef|grep -i solarwind
