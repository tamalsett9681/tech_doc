LPAR2RRD agent RPM's repository

LPAR2RRD agent feature description :
http://www.lpar2rrd.com/agent.htm

OS agent installation:
  - http://www.lpar2rrd.com/install.htm  (OS agent tab)

OS agent upgrade:
  - http://www.lpar2rrd.com/agent_upgrade.htm

OS agent release notes:
  - http://www.lpar2rrd.com/agent-relnotes.htm

NMON support:
  - http://www.lpar2rrd.com/nmon.htm
  - http://nmon.lpar2rrd.com

HMC monitoring
http://www.lpar2rrd.com/hmc_monitoring.htm
  - use this lpar2rrd-agent-4.60-0.Linux.noarch.rpm if your 
    LPAR2RRD server is on Intel linux



AIX: use AIX 5.1 RPM for ever newer AIX version

Linux: RPM is only for IBM Power platform
	lpar2rrd-agent-4.60-0.Linux.ppc.rpm

Installation example:
rpm -i lpar2rrd-agent-4.60-0.aix5.1.ppc.rpm

Upgrade example:
rpm -Uvh lpar2rrd-agent-4.60-0.aix5.1.ppc.rpm




Release notes:
==============
4.84-2 - HMC agent could stop working under some condition since 4.81 agent version, fixed 
4.84-1 - Fixed memory for some Linux X86 like CentOS 6 which had wrong data (negative). Most of Linux X86 have not been affected
4.84-0 - Linux X86 support 
         Corrected program return codes, when any error then agent process returns "1"
4.81-3 - when ethernet aliases were used (eth0:0) then it caused a parsing problems, translated to eth0-2
4.81-1 - 4.80 and 4.81 reported FCS only if there was any disk locally attached what NPIV and tape dedicated FSC are not 
4.81.0 - OS agent contacts the daemon randomly after 5 minutes to more spread connecting of the daemon in the time
       - 4.80 reported FCS only if there was any disk locally attached what NPIV and tape dedicated FSC are not
4.80.0 - final stable release
4.70.7 - it passes SMT info to the server
4.70.6 - all vSCSI adapters are monitored via iostat
         response times for all vSCSI and FCS adapters are monitored (requires LPAR2RRd server 4.74+)
4.70.4 - OS agent does not create core file, error log is recycled when its size > 1MB
4.70.3 - as prevetion against hanging external shell commands executed from the agent there is limited number of copies every particular external cmd running concurently in the system
4.70.2 - SEA fix (it did not sent SEA data for some NMON versions which use a bit different key word in the nmon file)
4.70.1 - introduced variable which preferes hostname before the lpar name in data send to the daemon (usefull for NMON loads from unmanaged full lpar partitions where is not posible to change lpar name)
       LPAR2RRD_HOSTNAME_PREFER=1 /usr/bin/perl /opt/lpar2rrd-agent/lpar2rrd-agent.pl -n /home/nmon_dir <lpar2rrd server>
4.70 - agent version is transfered within the data
4.69.4 - query of FC adapter in arbitrary loop cause excesive error loging, excluded them as they are only loopbacked
4.69 - fixed paging issue on AIX where paging of 64kB pages was considered as standard 4kB pages (real paging was higher than displayed)
4.68 - possibility to avoid svmon AIX cmd : 
       * * * * * GET_MEM="vmstat -v" /usr/bin/perl /opt/lpar2rrd-agent/lpar2rrd-agent.pl __LPAR2RRD-SERVER__ > /var/tmp/lpar2rrd-agent.out 2>&1
4.67 - fixed sending data to more LPAR2RRD servers, the bug has been there since 4.62
4.66 - fix for old AIX 5.2 and wrong reporting CPU util via vmstat (the first data line has to be ignored as it is summary since the OS start)
4.65 - NMON processing: when exist FCREAD FCWRITE in nmon file then it is prefered before IOADAPT which contains adapters only with disk (tapes are excluded)
4.64 - AIX 5.2 support
4.63 - removed warnings.pm Perl module dependency due to ancient AIX versions
4.62 - fixes for NMON online grapher
       Fixed "out of memory" when the data input files was for some reason too big
4.60 - it is implemented support for integrated NMON online grapher (aka http://nmon.lpar2rrd.com) in you local instance
4.50 - NMON data support
        It is able to use already generated NMON data as a source (online and batch mode processing is possible) instead of using agent called commands to get statistical data.
        It might use NMON data and its own data as the source concurrently.
        It is able to process collected NMON data from many LPARs in one location as a batch job once a day.
     - WPAR support
     - HMC monitoring
     - It posibble to feed via one OS agen more LPAR2RRD server concurently
4.07 - fixes FCS problems (it avoids non connected fcs adapters which causing problems like long term query and errpt warnings)
4.06 - it uses internal timestamp to save time of last saved data on the server side, then it sends newer data olny
       --> it re-solves situation when on the agent side is so much data that sending them overome internal timer (6mins)
4.05 - limit of concurently running OS agents to 10 on single OS (to save the OS if anything stuck)
     - possibility to exclude fcstat (SAN stats), it might cause a problems time to time
       crontab line:
       * * * * * FCSTAT=/bin/true; export FCSTAT; /usr/bin/perl /opt/lpar2rrd-agent/lpar2rrd-agent.pl __LPAR2RRD-SERVER__   __PORT__ > /var/tmp/lpar2rrd-agent.out 2>&1
4.04 - fix for LPARs (servers) not managed by the HMC, it has not worked before correctly
4.03 - fix for AME support, it did not work in 4.00 
     - fix for memory usage data if AMS is being used
4.00 - new comunication protocol (requires 4.00+ LPAR2RRD server)
     - data collection anhanced abour LAN (eth), SAN (fcs), SEA (VIOS only), paging allocated, AME
1.02 - initial release which supports memory and paging data only
